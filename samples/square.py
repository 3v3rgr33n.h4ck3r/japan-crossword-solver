from solver.JapInput import JapInput

output = """
xxxxx
x...x
x...x
xxxxx
""".strip()

inputs = JapInput(4, 5,
  [[5], [1, 1], [1, 1], [5]],
  [[4], [1, 1], [1, 1], [1, 1], [4]],
)
