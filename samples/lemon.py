from solver.JapInput import JapInput

output = """
.....xxxxxxxx.......
....xxx.....xx......
...xxx..xxxx.xx.....
..xx.x.x.xxx..xx....
.xxx.x.xx.xx.x.xx...
.xxx.x.xxx.x.xx.xx..
.x.x.x..xxxx.xxx.xx.
xx...xx......xxxx.x.
xx....x.xxxx......xx
x.....xx.xxx..xxx.xx
x......xx.xx.x.xxx.x
x.......xx.x.xx.xx.x
x........xx..xxx.x.x
x.........xx.xxxx.xx
xx.........xx..xxxxx
xx..xxxx....xxx..xxx
xxx....xxxx...xxxxx.
xxxxxx..xxxxxxxxx...
.xxxxxxxxxxxxxxx....
...xxxxxxxxxxx......
""".strip()

inputs = JapInput(
  20,
  20,
  [[8],[3,2],[3,4,2],[2,1,1,3,2],[3,1,2,2,1,2],[3,1,3,1,2,2],[1,1,1,4,3,2],[2,2,4,1],[2,1,4,2],[1,2,3,3,2],[1,2,2,1,3,1],[1,2,1,2,2,1],[1,2,3,1,1],[1,2,4,2],[2,2,5],[2,4,3,3],[3,4,5],[6,9],[15],[11]],
  [[11],[5,5],[3,3],[5,3],[2,1,3],[8,1,3],[2,3,1,2],[1,3,2,2,2],[1,1,3,1,2,4],[1,2,2,2,2,4],[1,3,1,3,2,4],[1,5,4,2,3],[2,2,3],[2,4,4,1,3],[2,3,1,3,4],[2,2,2,3,3],[2,1,3,2,2],[2,3,3],[4,4],[8]]
)