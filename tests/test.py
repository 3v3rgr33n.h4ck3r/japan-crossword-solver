from solver.JapInput import JapInput
from solver.PicToJap import convert_pic_to_jap
from solver.JapToPic import convert_jap_to_pic, gen_random_pic_lines, mutate_pic, jap_check_if_right, jap_success_weight
import samples.square

import unittest
class TestJapUtils(unittest.TestCase):
  def test_convert_pic_to_jap_empty(self):
    self.assertEqual(convert_pic_to_jap("""
          x
        xxxx
        xxx
      """).__repr__(), "'wrong format - should be a rectangle'")

  def test_convert_pic_to_jap(self):
    self.assertEqual(convert_pic_to_jap("""
        .x
        xx
      """).__repr__(), [
        2, 2,
        [[1],[2]],
        [[1],[2]]
      ])
    self.assertEqual(convert_pic_to_jap("""
      .xxx
      xxxx
    """).__repr__(), [
      2,4,
      [[3],[4]],
      [[1],[2],[2],[2]]
    ])
    self.assertEqual(convert_pic_to_jap("""
      .x.x
      x.xx
    """).__repr__(), [
      2,4,
      [[1,1],[1,2]],
      [[1],[1],[1],[2]]
    ])

class TestSolver(unittest.TestCase):
  def test_convert_jap_to_pic_empty(self):
    self.assertEqual(convert_jap_to_pic(""), "wrong input")

  def test_check_is_right_basic(self):
    jap = JapInput(1, 1, [[1]], [[1]])
    self.assertTrue(jap_check_if_right(jap, [['x']]))
    self.assertFalse(jap_check_if_right(jap, [['.']]))

  def test_check_is_right(self):
    jap = samples.square.inputs
    pic = samples.square.output
    pic_lines = [list(line.strip()) for line in pic.strip().split("\n")]

    self.assertTrue(jap_check_if_right(jap, pic_lines))
    pic_lines[0][0] = 'x' if pic_lines[0][0] == '.' else '.' 
    self.assertFalse(jap_check_if_right(jap, pic_lines))

  def test_jap_success_weight(self):
    jap = JapInput(1, 2, [[2]], [[1],[1]])
    self.assertTrue(jap_check_if_right(jap, [list('xx')]))
    self.assertFalse(jap_check_if_right(jap, [list('.x')]))
    self.assertFalse(jap_check_if_right(jap, [list('x.')]))
    self.assertFalse(jap_check_if_right(jap, [list('..')]))

    highest_to_lowest_actual = [
      jap_success_weight(jap, [list('xx')]),
      jap_success_weight(jap, [list('x.')]),
      jap_success_weight(jap, [list('.x')]),
      jap_success_weight(jap, [list('..')])
    ]
    highest_to_lowest_expected = sorted(highest_to_lowest_actual, reverse=True)
    self.assertEqual(highest_to_lowest_expected, highest_to_lowest_actual)

    self.assertGreater(highest_to_lowest_actual[0], highest_to_lowest_actual[1])
    self.assertGreater(highest_to_lowest_actual[1], highest_to_lowest_actual[3])

  # def test_convert_jap_to_pic(self):
  #   print("what should be:")
  #   print(samples.square.output)
  #   print("")

  #   print("what we have:")
  #   temp_jap = samples.square.inputs
  #   temp_pic = gen_random_pic_lines(temp_jap.rows_len, temp_jap.col_len)
  #   print("\n".join(["".join(x) for x in temp_pic]))
  #   print("")
    
  #   print("is right:", jap_check_if_right(temp_jap, temp_pic))

  #   print(temp_pic)
  #   self.assertEqual(convert_jap_to_pic(samples.square.inputs), samples.square.output)

  # def test_debug_how_solution_debug(self):
  #   (temp_rows_len, temp_col_len) = (2,4)
  #   temp_pic = gen_random_pic_lines(temp_rows_len, temp_col_len)
  #   print("\n".join(["".join(x) for x in temp_pic]))
  #   print("")
    
  #   temp_mutate_pic = mutate_pic(temp_rows_len, temp_col_len, temp_pic, 3)
  #   print("\n".join(["".join(x) for x in temp_mutate_pic]))
    
  #   temp_jap = JapInput(temp_rows_len, temp_col_len, 
  #     [[1,1],[1,2]],
  #     [[1],[1],[1],[2]])
  #   temp_mutate_pic = jap_check_if_right(temp_jap, temp_mutate_pic)
  #   print(temp_mutate_pic)


if __name__ == '__main__':
    unittest.main()