class JapInput:
  def __init__(self, rows_len, col_len, jap_left, jap_top):
    self.rows_len = rows_len
    self.col_len = col_len
    self.jap_left = jap_left
    self.jap_top = jap_top
  def __repr__(self):
    return [
      self.rows_len,
      self.col_len,
      self.jap_left,
      self.jap_top
    ]
    