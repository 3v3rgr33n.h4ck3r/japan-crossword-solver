from solver.JapInput import JapInput

def convert_pic_to_jap(pic):
  lines = [line.strip() for line in pic.strip().split("\n")]
  jap_left = []
  jap_top = []

  for line in lines:
    count = 0
    res = []
    for cell in line:
      if cell == '.':
        if count != 0:
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      res.append(count)
      count = 0
    jap_left.append(res)

  max_x = max([len(line) for line in lines])
  min_x = min([len(line) for line in lines])
  if max_x != min_x:
    return "wrong format - should be a rectangle"

  for col in range(max_x):
    count = 0
    res = []
    for line in lines:
      cell = line[col]
      if cell == '.':
        if count != 0:
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      res.append(count)
      count = 0
    jap_top.append(res)
  
  return JapInput(len(lines), max_x, jap_left, jap_top)