from solver.JapInput import JapInput


#%%
import random
def gen_random_pic_lines(rows_len, col_len):
  return [
    # "".join(
      [random.choice(['x', '.']) for x in range(col_len)]
    # )
    for y in range(rows_len)
  ]
# (temp_rows_len, temp_col_len) = (5,4)
# temp_pic = gen_random_pic_lines(temp_rows_len, temp_col_len)
# print("\n".join(["".join(x) for x in temp_pic]))
# print("")

def_mutations_number_max = 10
import copy
def mutate_pic(rows_len, col_len, pic_lines, mutations_number_max = def_mutations_number_max, mutate_static_number = False):
  ## TODO: optimize deepcopy in outer call
  pic_lines = copy.deepcopy(pic_lines)
  mutations_number = mutations_number_max if mutate_static_number else random.randint(1, mutations_number_max)
  for _ in range(mutations_number):
    col = random.randint(0, col_len-1)
    row = random.randint(0, rows_len-1)
    # TODO - remove dups (x,y)?
    pic_lines[row][col] = 'x' if pic_lines[row][col] == '.' else '.'
  return pic_lines

# temp_mutate_pic = mutate_pic(temp_rows_len, temp_col_len, temp_pic, 3)
# print("\n".join(["".join(x) for x in temp_mutate_pic]))


def jap_check_if_right(jap: JapInput, lines):
  cur_jap_line_index = 0
  for line in lines:
    count = 0
    res = []
    for cell in line:
      if cell == '.':
        if count != 0:
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      res.append(count)
      count = 0
    if jap.jap_left[cur_jap_line_index] != res:
      # print("in:", jap.jap_left, cur_jap_line_index)
      # print("out:", res)
      return False
    cur_jap_line_index += 1

  cur_jap_line_index = 0
  for col in range(len(lines[0])):
    count = 0
    res = []
    for line in lines:
      cell = line[col]
      if cell == '.':
        if count != 0:
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      res.append(count)
      count = 0
    if jap.jap_top[cur_jap_line_index] != res:
      # print("in:", jap.jap_top, cur_jap_line_index)
      # print("out:", res)
      return False
    cur_jap_line_index += 1

  return True

# temp_mutate_pic = jap_check_if_right(JapInput(temp_rows_len, temp_col_len, [1,10], [1,10]), temp_col_len, temp_pic, 3)
# print("\n".join(["".join(x) for x in temp_mutate_pic]))
#%%

def jap_success_weight(jap: JapInput, lines):

  def get_delta(a, i, ii, b):
    if i >= len(a):
      raise IndexError("%s is above %s" % (i, len(a)))
    if ii > len(a[i]):
      # raise IndexError("%s is above %s" % (ii, len(a[i])))
      a = 0
    else:
      a = a[i][ii]
    delta = abs(a - b)
    return delta

  negative_score = 0

  cur_jap_line_index = 0
  for line in lines:
    count = 0
    cur_jap_line_subindex = 0
    res = []
    for cell in line:
      if cell == '.':
        if count != 0:
          negative_score -= get_delta(jap.jap_left, cur_jap_line_index, cur_jap_line_subindex, count)
          cur_jap_line_subindex += 1
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      negative_score -= get_delta(jap.jap_left, cur_jap_line_index, cur_jap_line_subindex, count)
      cur_jap_line_subindex += 1
      res.append(count)
      count = 0
    if jap.jap_left[cur_jap_line_index] != res:
      negative_score -= abs(len(jap.jap_left[cur_jap_line_index]) - len(res))
    cur_jap_line_index += 1

  cur_jap_line_index = 0
  for col in range(len(lines[0])):
    count = 0
    cur_jap_line_subindex = 0
    res = []
    for line in lines:
      cell = line[col]
      if cell == '.':
        if count != 0:
          negative_score -= get_delta(jap.jap_top, cur_jap_line_index, cur_jap_line_subindex, count)
          cur_jap_line_subindex += 1
          res.append(count)
          count = 0
      elif cell == 'x':
        count += 1
    if count != 0:
      negative_score -= get_delta(jap.jap_top, cur_jap_line_index, cur_jap_line_subindex, count)
      cur_jap_line_subindex += 1
      res.append(count)
      count = 0
    if jap.jap_top[cur_jap_line_index] != res:
      negative_score -= abs(len(jap.jap_top[cur_jap_line_index]) - len(res))
    cur_jap_line_index += 1

  return negative_score


#TODO leave the "frontier" of winners, not only 1 from the generation
#TODO remove the the frontier only after X generations
jap_initial_gen_numbers = 10
def convert_jap_to_pic(jap: JapInput):
  if type(jap) != JapInput:
    return "wrong input"
  pic_lines_arr = []
  max_weight = None
  for i in range(jap_initial_gen_numbers):
    pic_lines = gen_random_pic_lines(jap.rows_len, jap.col_len)
    pic_lines_weight = jap_success_weight(jap, pic_lines)
    pic_lines_arr.append({"lines": pic_lines, "weight": pic_lines_weight})
    if max_weight is None or max_weight["value"] < pic_lines_weight:
      max_weight = {
        "value": pic_lines_weight,
        "lines": pic_lines,
        "index": i
        }

  # while
  print(pic_lines_arr)
  print(max_weight)

